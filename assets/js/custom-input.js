// #### ADD CLASS RESPONSIVE ####

// UIKIT SLIDER
$(window).on('load', function() {
    if($(window).width() < 911) {
        $('.slider-m-mod li').addClass('uk-width-4-5');
        //$('.section-partner ul.nav').addClass('nav-justified');
    }
});

// Box SLider
$(window).on('load', function() {
    if($(window).width() < 911) {
        $('.pop-modal').addClass('modal');
        $('.pop-modal').attr('id', 'ModalFilter');
        //$('.section-partner ul.nav').addClass('nav-justified');
    }
});

$(document).ready(function(){
    $(".close").click(function(){
      $("#ModalFilter").modal('hide');
  });
});

// ###### Call popover #####

$("[data-toggle=popover]").popover({
    container: 'body',
    trigger: 'hover',
    placement: 'bottom',
    html: true
})



// #### SLIDE JUMLAH ####
"use strict";

(function ($) {
  var rangeWrapper = '.range-field';
  var rangeType = 'input[type=range]:not(.custom-range):not(.multi-range)';
  var thumbHtml = '<span class="thumb"><span class="value"></span></span>';
  var rangeMousedown = false;
  var left;

  function addThumb() {
    var $thumb = $(thumbHtml);
    $(rangeType).after($thumb);
  }

  $(document).on('change', rangeType, function () {
    var $thumb = $(this);
    var $thumbValue = $thumb.siblings('.thumb').find('.value');
    $thumbValue.html($thumb.val());
  });
  $(document).on('input mousedown touchstart', rangeType, function (e) {
    var $this = $(this);
    var $thumb = $this.siblings('.thumb');
    var width = $this.outerWidth();
    var noThumb = !$thumb.length;

    if (noThumb) {
      addThumb();
    } // Set indicator value


    $thumb.find('.value').html($this.val());
    rangeMousedown = true;
    $this.addClass('active');

    if (!$thumb.hasClass('active')) {
      $thumb.velocity({
        height: '30px',
        width: '30px',
        top: '-20px',
        marginLeft: '-15px'
      }, {
        duration: 300,
        easing: 'easeOutExpo'
      });
    }

    if (e.type !== 'input') {
      var isMobile = e.pageX === undefined || e.pageX === null;

      if (isMobile) {
        left = e.originalEvent.touches[0].pageX - $(this).offset().left;
      } else {
        left = e.pageX - $(this).offset().left;
      }

      if (left < 0) {
        left = 0;
      } else if (left > width) {
        left = width;
      }

      $thumb.addClass('active').css('left', left);
    }

    $thumb.find('.value').html($this.val());
  });
  $(document).on('mouseup touchend', rangeWrapper, function () {
    rangeMousedown = false;
    $(this).removeClass('active');
  });
  $(document).on('mousemove touchmove', rangeWrapper, function (e) {
    var $thumb = $(this).children('.thumb');
    var left;

    if (rangeMousedown) {
      if (!$thumb.hasClass('active')) {
        $thumb.velocity({
          height: '30px',
          width: '30px',
          top: '-20px',
          marginLeft: '-15px'
        }, {
          duration: 300,
          easing: 'easeOutExpo'
        });
      }

      var isMobile = e.pageX === undefined || e.pageX === null;

      if (isMobile) {
        left = e.originalEvent.touches[0].pageX - $(this).offset().left;
      } else {
        left = e.pageX - $(this).offset().left;
      }

      var width = $(this).outerWidth();

      if (left < 0) {
        left = 0;
      } else if (left > width) {
        left = width;
      }

      $thumb.addClass('active').css('left', left);
      $thumb.find('.value').html($thumb.siblings(rangeType).val());
    }
  });
  $(document).on('mouseout touchleave', rangeWrapper, function () {
    if (!rangeMousedown) {
      var $thumb = $(this).children('.thumb');

      if ($thumb.hasClass('active')) {
        $thumb.velocity({
          height: '0',
          width: '0',
          top: '10px',
          marginLeft: '-6px'
        }, {
          duration: 100
        });
      }

      $thumb.removeClass('active');
    }
  });
})(jQuery);


$(document).ready(function(){
  $("#b-register").click(function() {
    $(".d-login").removeClass("d-block").addClass("d-none");
    $(".d-register").removeClass("d-none").addClass("d-block");
  });
  
  $("#b-login").click(function() {
    $(".d-login").removeClass("d-none").addClass("d-block");
    $(".d-register").removeClass("d-block").addClass("d-none");
  });

  $("#btnTriggerPrivacy").click(function () {
    $("#ModalLogin").modal("hide");
    $("#ModalPrivacy").modal("show");
  });

  $("#btnTriggerOtp").click(function () {
    $("#ModalPrivacy").modal("hide");
    $("#ModalOtp").modal("show");
  });
  $("#btnTriggerOtpSuccess").click(function () {
    $("#ModalOtp").modal("hide");
    $("#ModalOtpSuccess").modal("show");
  });
});

// #### OTP INPUT ####
$(document).ready(function(){
  $('.digit-group').find('input').each(function() {
    $(this).attr('maxlength', 1);
    $(this).on('keyup', function(e) {
      var parent = $($(this).parent());
      
      if(e.keyCode === 8 || e.keyCode === 37) {
        var prev = parent.find('input#' + $(this).data('previous'));
        
        if(prev.length) {
          $(prev).select();
        }
      } else if((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
        var next = parent.find('input#' + $(this).data('next'));
        
        if(next.length) {
          $(next).select();
        } else {
          if(parent.data('autosubmit')) {
            parent.submit();
          }
        }
      }
    });
  });
});


// #### SWIPER JS ####

$(document).ready(function(){
  var swiper = new Swiper('.swiper-container', {
    effect: 'fade',
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
  });
});

// #### Range Slider ####

var $range = $(".js-range-slider"),
    $inputFrom = $(".js-input-from"),
    $inputTo = $(".js-input-to"),
    instance,
    min = 0,
    max = 4000000,
    from = 0,
    to = 4000000;

$range.ionRangeSlider({
  skin: "round",
    type: "double",
    hide_min_max: "true",
    min: min,
    max: max,
    from: 0,
    to: 4000000,
    step: 100000,
    onStart: updateInputs,
    onChange: updateInputs
});
instance = $range.data("ionRangeSlider");

function updateInputs (data) {
  from = data.from;
    to = data.to;
    
    $inputFrom.prop("value", from);
    $inputTo.prop("value", to); 
}

$inputFrom.on("change", function () {
    var val = $(this).prop("value");

    // validate
    if (val < min) {
        val = min;
    } else if (val > to) {
        val = to;
    }

    instance.update({
        from: val
    });

    $(this).prop("value", val);

});

$inputTo.on("change", function () {
    var val = $(this).prop("value");

    // validate
    if (val < from) {
        val = from;
    } else if (val > max) {
        val = max;
    }

    instance.update({
        to: val
    });

    $(this).prop("value", val);
});

// #### Rsnge Slider ####

var $range2 = $(".js-range-slider2"),
    $inputFrom2 = $(".js-input-from2"),
    $inputTo2 = $(".js-input-to2"),
    instance,
    min = 0,
    max = 4000000,
    from = 0,
    to = 4000000;

$range2.ionRangeSlider({
  skin: "round",
    type: "double",
    hide_min_max: "true",
    min: min,
    max: max,
    from: 0,
    to: 4000000,
    step: 100000,
    onStart: updateInputs2,
    onChange: updateInputs2
});
instance2 = $range2.data("ionRangeSlider");

function updateInputs2 (data) {
  from = data.from;
    to = data.to;
    
    $inputFrom2.prop("value", from);
    $inputTo2.prop("value", to); 
}

$inputFrom2.on("change", function () {
    var val = $(this).prop("value");

    // validate
    if (val < min) {
        val = min;
    } else if (val > to) {
        val = to;
    }

    instance2.update({
        from: val
    });

    $(this).prop("value", val);

});

$inputTo2.on("change", function () {
    var val = $(this).prop("value");

    // validate
    if (val < from) {
        val = from;
    } else if (val > max) {
        val = max;
    }

    instance2.update({
        to: val
    });

    $(this).prop("value", val);
});


// ###### INPUT RUPIAH #####
var rupiah = document.getElementById("ExamplePinjaman");
rupiah.addEventListener("keyup", function(e) {
  rupiah.value = formatRupiah(this.value, "Rp. ");
});

function formatRupiah(angka, prefix) {
  var number_string = angka.replace(/[^,\d]/g, "").toString(),
    split = number_string.split(","),
    sisa = split[0].length % 3,
    rupiah = split[0].substr(0, sisa),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

  if (ribuan) {
    separator = sisa ? "." : "";
    rupiah += separator + ribuan.join(".");
  }

  rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
  return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
}
